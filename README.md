# is-string

[![build status](https://gitlab.com/ci/projects/31185/status.png?ref=master)](https://gitlab.com/ci/projects/31185?ref=master)

string type checker

```
npm install @amphibian/is-string
```

```javascript
var isString = require('@amphibian/is-string');

isString(''); // > true
isString(true); // > false
```
