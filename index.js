// ---------------------------------------------------------------------------
//  is-string
//
//  string type checker
//  - variable: Variable to be checked
// ---------------------------------------------------------------------------

    function isString(variable) {
        return typeof variable === 'string';
    }

    module.exports = isString;
